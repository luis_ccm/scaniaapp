package com.scania.scaniaapp.di

import com.scania.scaniaapp.feature.countries.di.CountriesModule

object AppModules {
    val module = listOf(RemoteModule.module, CountriesModule.module)
}