package com.scania.scaniaapp.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RemoteModule {
    private const val url = "https://restcountries.com/v3.1/"

    val module = module {
        single<Gson> {
            GsonBuilder().create()
        }

        single<Retrofit> {
            Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}