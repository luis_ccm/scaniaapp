package com.scania.scaniaapp.feature.countries.di

import com.scania.core.coroutine.CoroutineProvider
import com.scania.scaniaapp.feature.countries.data.service.CountriesService
import com.scania.scaniaapp.feature.countries.domain.CountriesRepository
import com.scania.scaniaapp.feature.countries.domain.CountriesRepositoryImpl
import com.scania.scaniaapp.feature.countries.ui.CountriesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object CountriesModule {
    val module = module {
        single {
            get<Retrofit>().create(CountriesService::class.java)
        }

        single {
            CoroutineProvider()
        }

        single<CountriesRepository> {
            CountriesRepositoryImpl(service = get(), coroutineDispatcher = get())
        }

        viewModel {
            CountriesViewModel(repository = get())
        }
    }
}