package com.scania.scaniaapp.feature.countries.data.service

import com.scania.scaniaapp.feature.countries.data.model.CountryInfoResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface CountriesService {
    @GET("all")
    suspend fun getCountriesList(): List<CountryInfoResponse>
}
