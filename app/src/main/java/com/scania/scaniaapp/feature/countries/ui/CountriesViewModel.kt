package com.scania.scaniaapp.feature.countries.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.scania.core.extensions.onFailure
import com.scania.core.extensions.onSuccess
import com.scania.core.extensions.toLiveData
import com.scania.core.util.SingleLiveDataEvent
import com.scania.scaniaapp.feature.countries.domain.CountriesRepository
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel
import kotlinx.coroutines.launch

class CountriesViewModel(private val repository: CountriesRepository): ViewModel() {

    private val _loadingLiveData: MutableLiveData<Unit> = SingleLiveDataEvent()
    val loadingLiveData = _loadingLiveData.toLiveData()

    private val _errorLiveData: MutableLiveData<Exception> = SingleLiveDataEvent()
    val errorLiveData = _errorLiveData.toLiveData()

    private val _successLiveData: MutableLiveData<List<CountryInfoModel>> = SingleLiveDataEvent()
    val successLiveData = _successLiveData.toLiveData()

    fun getCountries() {
        viewModelScope.launch {
            _loadingLiveData.value = Unit
            val result = repository.getCountriesList()
            result.onSuccess {
                _successLiveData.value = it
            }.onFailure {
                _errorLiveData.value = it
            }
        }
    }
}