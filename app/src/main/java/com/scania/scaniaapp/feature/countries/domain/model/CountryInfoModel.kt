package com.scania.scaniaapp.feature.countries.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class CountryInfoModel(
    val name: CountryNameModel,
    val olympicCommitteeCode: String,
    val isIndependent: Boolean,
    val unMember: Boolean,
    val capitalList: List<String> = emptyList(),
    val altSpellings: List<String> = emptyList(),
    val region: String,
    val subRegion: String,
    val borders: List<String> = emptyList(),
    val area: BigDecimal,
    val flag: String
): Parcelable
