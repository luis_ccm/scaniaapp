package com.scania.scaniaapp.feature.countries.domain

import com.scania.core.data.State
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel

interface CountriesRepository {
    suspend fun getCountriesList() : State<List<CountryInfoModel>>
}