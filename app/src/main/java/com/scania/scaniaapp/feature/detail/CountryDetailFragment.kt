package com.scania.scaniaapp.feature.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.scania.core.extensions.viewBinding
import com.scania.scaniaapp.R
import com.scania.scaniaapp.databinding.FragmentCountryDetailBinding
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel

class CountryDetailFragment : Fragment() {

    private var binding: FragmentCountryDetailBinding by viewBinding()
    private val country: CountryInfoModel by lazy {
        val args: CountryDetailFragmentArgs by navArgs()
        args.country
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCountryDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showInfo()
    }

    private fun showInfo() {
        binding.run {
            countryDetailFlag.text = country.flag
            countryDetailOfficialName.text =
                getString(R.string.country_name_label, country.name.officialName)
            countryDetailCommonName.text =
                getString(R.string.country_common_name_label, country.name.commonName)
            countryDetailOlympicName.text =
                getString(R.string.country_olympic_label, country.olympicCommitteeCode)

            countryDetailIndependent.isVisible = country.isIndependent
            countryDetailUnMember.isVisible = country.unMember

            val capitals = country.capitalList.concatenateWithComma()
            countryDetailCapital.text = getString(R.string.country_capitals_label, capitals)

            val altSpellings = country.altSpellings.concatenateWithComma()
            countryDetailAltSpellings.text = getString(R.string.country_alt_spellings_label, altSpellings)

            countryDetailRegion.text = getString(R.string.country_region_label, country.region)
            countryDetailSubRegion.text = getString(R.string.country_sub_region_label, country.subRegion)

            countryDetailBorders.text = getString(R.string.country_borders_label, country.borders)
            countryDetailArea.text = getString(R.string.country_area_label, country.area.toString())
        }
    }
}

private fun List<String>.concatenateWithComma(): String = this.joinToString { "\'${it}\'" }