package com.scania.scaniaapp.feature.countries.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.scania.core.extensions.viewBinding
import com.scania.scaniaapp.databinding.FragmentCountriesListBinding
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel
import com.scania.scaniaapp.feature.countries.ui.adapter.CountriesListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class CountriesListFragment : Fragment() {

    private var binding by viewBinding<FragmentCountriesListBinding>()
    private val viewModel: CountriesViewModel by viewModel()
    private val adapter: CountriesListAdapter = CountriesListAdapter {
        navigateToCountryDetail(it)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCountriesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureView()
        observeResults()
        viewModel.getCountries()
    }

    private fun configureView() {
        binding.countriesList.adapter = adapter
    }

    private fun observeResults() {
        viewModel.loadingLiveData.observe(viewLifecycleOwner) {
            showLoading()
        }

        viewModel.errorLiveData.observe(viewLifecycleOwner) {
            showError()
        }

        viewModel.successLiveData.observe(viewLifecycleOwner) {
            showList(it)
        }
    }

    private fun showLoading() {
        binding.countriesProgress.isVisible = true
        binding.countriesList.isVisible = false
        binding.countriesError.root.isVisible = false
    }

    private fun showError() {
        binding.countriesProgress.isVisible = false
        binding.countriesList.isVisible = false
        binding.countriesError.root.isVisible = true
    }

    private fun showList(countries: List<CountryInfoModel>) {
        adapter.submitList(countries)
        binding.countriesProgress.isVisible = false
        binding.countriesList.isVisible = true
        binding.countriesError.root.isVisible = false
    }

    private fun navigateToCountryDetail(countryInfoModel: CountryInfoModel) {
        findNavController().navigate(
            CountriesListFragmentDirections.actionCountriesFragmentToCountryDetailFragment(
                countryInfoModel
            )
        )
    }
}