package com.scania.scaniaapp.feature.countries.data.model

import com.google.gson.annotations.SerializedName

data class CountryNameResponse(
    @SerializedName("common")
    val commonName: String,
    @SerializedName("official")
    val officialName: String
)
