package com.scania.scaniaapp.feature.countries.data.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CountryInfoResponse(
    @SerializedName("name")
    val name: CountryNameResponse,
    @SerializedName("cioc")
    val olympicCommitteeCode: String?,
    @SerializedName("independent")
    val isIndependent: Boolean,
    @SerializedName("unMember")
    val unMember: Boolean,
    @SerializedName("capital")
    val capitalList: List<String>?,
    @SerializedName("altSpellings")
    val altSpellings: List<String>,
    @SerializedName("region")
    val region: String,
    @SerializedName("subregion")
    val subRegion: String?,
    @SerializedName("borders")
    val borders: List<String>?,
    @SerializedName("area")
    val area: BigDecimal,
    @SerializedName("flag")
    val flag: String?
)