package com.scania.scaniaapp.feature.countries.data.model

import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel
import com.scania.scaniaapp.feature.countries.domain.model.CountryNameModel

fun CountryInfoResponse.mapToDomain(): CountryInfoModel {
    return CountryInfoModel(
        name = this.name.mapToDomain(),
        olympicCommitteeCode = this.olympicCommitteeCode.orEmpty(),
        isIndependent = isIndependent,
        unMember = unMember,
        capitalList = this.capitalList ?: emptyList(),
        altSpellings = this.altSpellings,
        region = this.region,
        subRegion = this.subRegion.orEmpty(),
        borders = this.borders ?: emptyList(),
        area = this.area,
        flag = this.flag.orEmpty(),
    )
}
private fun CountryNameResponse.mapToDomain(): CountryNameModel {
    return CountryNameModel(
        commonName = this.commonName,
        officialName = this.officialName
    )
}