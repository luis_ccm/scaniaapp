package com.scania.scaniaapp.feature.countries.domain

import com.scania.core.coroutine.CoroutineProvider
import com.scania.core.data.State
import com.scania.scaniaapp.feature.countries.data.model.CountryInfoResponse
import com.scania.scaniaapp.feature.countries.data.model.mapToDomain
import com.scania.scaniaapp.feature.countries.data.service.CountriesService
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class CountriesRepositoryImpl(
    private val service: CountriesService,
    private val coroutineDispatcher: CoroutineProvider
) : CountriesRepository {

    override suspend fun getCountriesList(): State<List<CountryInfoModel>> {
        return coroutineScope {
            withContext(coroutineDispatcher.ioDispatcher) {
                try {
                    State.Success(
                        service.getCountriesList().map { it.mapToDomain() })
                } catch (exception: Exception) {
                    State.Failure(exception)
                }
            }
        }
    }
}