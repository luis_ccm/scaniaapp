package com.scania.scaniaapp.feature.home.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.scania.core.extensions.viewBinding
import com.scania.scaniaapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var binding by viewBinding<FragmentHomeBinding>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.homeContinueButton.setOnClickListener {
            navigateToCountriesScreen()
        }
    }

    private fun navigateToCountriesScreen() {
        findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToCountriesFragment())
    }
}