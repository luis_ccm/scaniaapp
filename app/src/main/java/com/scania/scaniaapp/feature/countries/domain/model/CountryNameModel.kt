package com.scania.scaniaapp.feature.countries.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CountryNameModel(
    val commonName: String,
    val officialName: String
) : Parcelable
