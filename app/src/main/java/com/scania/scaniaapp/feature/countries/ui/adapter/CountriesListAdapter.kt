package com.scania.scaniaapp.feature.countries.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.scania.scaniaapp.R
import com.scania.scaniaapp.databinding.CountryListItemBinding
import com.scania.scaniaapp.feature.countries.domain.model.CountryInfoModel

class CountriesListAdapter(
    private val onItemClick: (CountryInfoModel) -> Unit,
) : ListAdapter<CountryInfoModel, CountriesListAdapter.CountryViewHolder>(object :
    DiffUtil.ItemCallback<CountryInfoModel>() {
    override fun areItemsTheSame(oldItem: CountryInfoModel, newItem: CountryInfoModel): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(
        oldItem: CountryInfoModel,
        newItem: CountryInfoModel
    ): Boolean {
        return oldItem == newItem
    }

}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val itemBinding =
            CountryListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CountryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CountryViewHolder(private val item: CountryListItemBinding) :
        RecyclerView.ViewHolder(item.root) {
        fun bind(country: CountryInfoModel) {
            item.countryItemFlag.text = country.flag
            itemView.context.run {
                item.countryItemName.text = getString(R.string.country_name_label, country.name.officialName)
                item.countryItemCapital.text = getString(
                    R.string.country_capital_label,
                    country.capitalList.firstOrNull().orEmpty()
                )
                item.countryItemRegion.text = getString(R.string.country_region_label, country.region)
            }
            item.root.setOnClickListener {
                onItemClick.invoke(country)
            }
        }
    }
}
