package com.scania.scaniaapp.application

import android.app.Application
import com.scania.scaniaapp.di.AppModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class AppApplication: Application()  {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            loadKoinModules(AppModules.module)
        }
    }
}