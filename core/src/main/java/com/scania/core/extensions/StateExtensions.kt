package com.scania.core.extensions

import com.scania.core.data.State


inline fun <T> State<T>.onSuccess(action: (value: T) -> Unit): State<T> {
    if (this is State.Success) {
        action.invoke(data)
    }
    return this
}

inline fun <T> State<T>.onFailure(action: (value: Exception) -> Unit): State<T> {
    if (this is State.Failure) {
        action.invoke(error)
    }
    return this
}