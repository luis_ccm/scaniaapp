package com.scania.core.data

sealed class State<out T> {
    data class Success<T>(val data: T) : State<T>()
    data class Failure(val error: Exception) : State<Nothing>()
}